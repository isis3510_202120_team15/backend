import * as express from 'express'
import { databaseConnection, encontrarElementoMasFrecuente } from './utils'

const [dbRestaurantes, dbReservas] =
[databaseConnection.collection('restaurantes'),
  databaseConnection.collection('reserva')]

export const recomendacionesManager = express()

recomendacionesManager.get('/:idUsuario', async (req, res) => {
  const idCliente = req.params.idUsuario
  const reservasCliente = (await dbReservas.where('clienteId', '==', idCliente).get()).docs.map(snapshot => snapshot.data()).map(reserva => reserva.horaReserva)
  const horarioPreferido = encontrarElementoMasFrecuente(reservasCliente)
  const restaurantesConReservas = (await dbRestaurantes.where('horariosReserva', 'array-contains', horarioPreferido).get()).docs.map(snapshot => snapshot.data())
  return res.status(200).send(restaurantesConReservas)
})
