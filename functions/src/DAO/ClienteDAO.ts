/**
 * DAO class for Cliente
 */
import {
  databaseConnection,
  regex,
  error,
  dateUtils,
  search,
  generateRandomId,
  searchIndex
} from './utils'
import * as Joi from 'joi'
import * as express from 'express'

const DOC_TYPE = ['C.C', 'C.E', 'T.I']

const db = databaseConnection.collection('clientes')

/**
 * Express App for handeling request
 */
export const ClienteDAO = express()

/**
 * Joi clientSchema
 */
const clientSchema = Joi.object({
  id: Joi.string()
    .required()
    .min(0),
  nombre: Joi.string()
    .required()
    .max(100)
    .min(5)
    .pattern(regex('^[A-Z|a-z| ]*$'))
    .error(error('Nombre no valido')),
  correo: Joi.string().required().email().error(error('Correo no valido')),
  telefono: Joi.string()
    .required()
    .length(10)
    .pattern(regex('^[0-9]*$'))
    .error(error('Telefono no valido')),
  tipoDoc: Joi.string()
    .required()
    .valid(...DOC_TYPE)
    .error(error('Tipo de documento no valido')),
  documento: Joi.string()
    .required()
    .min(7)
    .pattern(regex('^[0-9]*$'))
    .error(error('Documento no valido')),
  tarjetas: Joi.array()
    .required()
    .min(0)
    .error(error('No hay propiedad de tarjetas')),
  pedidos: Joi.array()
    .required()
    .min(0)
    .error(error('No hay propiedad para los pedidos')),
  foto: Joi.string()
    .required()
    .uri()
    .error(error('Direccion a la foto no es valdia'))
})

/**
 * JOI Eschema for validation
 */
const cardSchema = Joi.object({
  cardNumber: Joi.string()
    .required()
    .creditCard()
    .error(error('Tarjeta no valida')),
  fecha: Joi.date().required().greater('now').error(error('Tarjeta Expirada')),
  cardHolder: Joi.string()
    .required()
    .max(100)
    .min(5)
    .pattern(regex('^[A-Z|a-z| ]*$'))
    .error(error('Nombre no valido'))
})

/**
 * GET ALL Method for Clients
 */
ClienteDAO.get('/', (req, res) => {
  const docEmail = req.query.email
  if (docEmail) {
    getByEmail(docEmail, res)
  } else {
    db.get().then((snapshot) =>
      res.status(200).send(snapshot.docs.map(doc => { return { ...doc.data(), id: doc.id } }))
    )
  }
})
/**
 * GET BY ID Method for Clients
 */
ClienteDAO.get('/:id', (req, res) => {
  const docId = req.params.id
  db.doc(docId)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return res.status(200).send({ ...doc.data(), id: doc.id })
      } else {
        return res.status(404).send({ error: 'El cliente no existe' })
      }
    })
})

/**
 * GET BY EMAIL Method for Client
 */
const getByEmail = (docEmail:any, res:any) => {
  db.where('correo', '==', docEmail)
    .get()
    .then((snapshot) => {
      if (!snapshot.empty) {
        const doc = snapshot.docs[0]
        return res.status(200).send({ ...doc.data(), id: doc.id })
      } else {
        return res.status(400).send({ error: 'No se encontro un cliente con ese correo' })
      }
    })
}

const validarUnicoTelefono = async (telefono:string) => {
  return (await db.where('telefono', '==', telefono).get()).empty
}

const validarUnicoCorreo = async (correo:string) => {
  return (await db.where('correo', '==', correo).get()).empty
}

const validarUnicoDocumento = async (documento:string) => {
  return (await db.where('documento', '==', documento).get()).empty
}

/**
 * POST Methof for Clients
 */
ClienteDAO.post('/', async (req, res) => {
  const data = req.body
  const clienteDoc = data.documento.trim()
  const telefonoCliente = data.telefono.trim()
  const correoCliente = data.correo.trim()
  const { error } = clientSchema.validate(data)
  if (!error) {
    switch (false) {
      case await validarUnicoDocumento(clienteDoc): {
        console.log('Error validar doc')
        return res.status(400).send({ error: 'Existe un cliente con el documento dado' })
      }
      case await validarUnicoTelefono(telefonoCliente): {
        console.log('Error validar telefono')
        return res.status(400).send({ error: 'Existe un cliente con el correo telefono' })
      }
      case await validarUnicoCorreo(correoCliente): {
        console.log('Error validar correo')
        return res.status(400).send({ error: 'Existe un cliente con el correo dado' })
      }
      default: {
        const id = await (await db.add(data)).id
        return res.status(200).send({ ...data, id: id })
      }
    }
  } else {
    console.log('Error validar parametros: ', error.message)
    return res.status(400).send({ error: error.message })
  }
})

/**
 * PUT method for client
 */
ClienteDAO.put('/:id', (req, res) => {
  const docId = req.params.id
  let data = req.body
  db.doc(docId)
    .get()
    .then((doc) => {
      if (doc.exists) {
        const { error } = clientSchema.validate(data)
        if (!error) {
          data = db.doc(docId).update(data)
          console.log(data)
          return res.status(200).send({ ...data, id: docId })
        } else {
          console.log('Error')
          return res.status(400).send({ error: error.message })
        }
      } else {
        console.log('Error')
        return res.status(404).send({ error: 'Cliente no encontrado' })
      }
    })
})

/**
 * DELETE method for client
 */
ClienteDAO.delete('/:id', (req, res) => {
  const docId = req.params.id
  db.doc(docId)
    .get()
    .then((doc) => {
      if (doc.exists) {
        db.doc(docId).delete()
        console.log('Delete')
        return res.status(200).send('Cliente eliminado de la base de datos')
      } else {
        console.log('No Delete')
        return res.status(404).send({ error: 'Cliente no encontrado' })
      }
    })
})

/**
 * GET ALL Credit cards of a client
 */
ClienteDAO.get('/:clientId/tarjeta', (req, res) => {
  const docId = req.params.clientId
  db.doc(docId)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return res.status(200).send(doc.data()?.tarjetas.map((tarjeta:any) => { return { ...tarjeta, id: tarjeta.id } }))
      } else {
        return res.status(404).send({ error: 'Cliente no encontrado' })
      }
    })
})

/**
 * GET BY ID Credit cards of a client
 */

const validarClienteExiste = async (idCliente:any) => {
  return (await db.doc(idCliente).get()).exists
}

const obtenerCliente = async (idCliente:any) => {
  return (await db.doc(idCliente).get()).data()
}

ClienteDAO.get('/:clientId/tarjeta/:tarjetaId', async (req, res) => {
  const docId = req.params.clientId
  const cardId = req.params.tarjetaId
  const client = await obtenerCliente(docId)
  const tarjetas = client?.tarjetas
  const tarjeta = search(cardId, tarjetas)
  if (await validarClienteExiste(docId)) {
    if (!tarjeta) {
      return res.status(400).send({ error: 'Cliente no encontrado' })
    } else {
      return res
        .status(200)
        .send({ ...tarjeta, id: tarjeta.id })
    }
  } else {
    return res.status(400).send({ error: 'Cliente no encontrado' })
  }
})

/**
 * POST Method for Tarjeta
 */
const validarExisteTarjeta = async (tarjetas:any, number:string) => {
  if (tarjetas.length > 0) {
    tarjetas = tarjetas.map((x: any) => x.cardNumber)
    return tarjetas.indexOf(number) === -1
  } else {
    return true
  }
}

ClienteDAO.post('/:clientId/tarjeta/', async (req, res) => {
  const docId = req.params.clientId
  const client = await obtenerCliente(docId)
  const tarjetas = client?.tarjetas
  const data = JSON.parse(req.body.data)
  data.fecha = dateUtils.createDateFromExpDate(data.fecha)
  const { error } = cardSchema.validate(data)
  if (!error) {
    switch (false) {
      case await validarClienteExiste(docId): {
        return res.status(400).send('Cliente no existe')
      }
      case await validarExisteTarjeta(tarjetas, data.cardNumber): {
        return res.status(400).send('Esta tarjeta ya se encuentra registrada')
      }
      default: {
        const cardId = generateRandomId()
        tarjetas.push({ ...data, id: cardId })
        db.doc(docId).update({ tarjetas: tarjetas })
        return res.status(200).send({ message: 'Tarjeta creada exitosamente', data: { ...data, id: cardId } })
      }
    }
  } else {
    return res.status(400).send(error.message)
  }
})

/**
 * PUT Method for Credit Card
 */
ClienteDAO.put('/:clientId/tarjeta/:tarjetaId', async (req, res) => {
  const docId = req.params.clientId
  const cardId = req.params.tarjetaId
  const client = await obtenerCliente(docId)
  const tarjetas = client?.tarjetas
  const tarjeta = search(cardId, tarjetas)
  const data = JSON.parse(req.body.data)
  data.fecha = dateUtils.createDateFromExpDate(data.fecha)
  const { error } = cardSchema.validate(data)
  if (await validarClienteExiste(docId)) {
    if (!error) {
      if (!tarjeta) {
        return res.status(400).send('Cliente no encontrado')
      } else {
        const index = searchIndex(cardId, tarjetas)
        tarjetas[index] = { ...data, id: cardId }
        db.doc(docId).update({ tarjetas: tarjetas })
        return res.status(200).send('Tarjeta de credito actualizada')
      }
    } else {
      return res.status(400).send(error.message)
    }
  } else {
    return res.status(400).send('Cliente no encontrado')
  }
})

/**
 * DELETE Method for Credit Card
 */
ClienteDAO.delete('/:clientId/tarjeta/:tarjetaId', async (req, res) => {
  const docId = req.params.clientId
  const cardId = req.params.tarjetaId
  const client = await obtenerCliente(docId)
  const tarjetas = client?.tarjetas
  const tarjeta = search(cardId, tarjetas)
  if (await validarClienteExiste(docId)) {
    if (!tarjeta) {
      return res.status(400).send('Cliente no encontrado')
    } else {
      db.doc(docId).update({
        tarjetas: tarjetas.splice(searchIndex(cardId, tarjetas), 1)
      })
      return res.status(200).send('Tarjeta de credito eliminada')
    }
  } else {
    return res.status(400).send('Cliente no encontrado')
  }
})
