/**
 * DAO class for Reserva
 */
import { databaseConnection, error, dateUtils, regex } from './utils'
import * as Joi from 'joi'
import * as express from 'express'

const dbReservas = databaseConnection.collection('reserva')
const dbClientes = databaseConnection.collection('clientes')
const dbRestaurantes = databaseConnection.collection('restaurantes')

export const ReservasDAO = express()

/**
 * JOI Schema
 */
const reservaSchema = Joi.object({
  clienteId: Joi.string()
    .required()
    .alphanum()
    .error(error('Id del cliente no valida')),
  restauranteId: Joi.string()
    .required()
    .alphanum()
    .error(error('Id del restaurante no valido')),
  platos: Joi.array()
    .items(Joi.string())
    .required()
    .min(0)
    .error(error('Los id de los platos no son validos')),
  diaReserva: Joi.string()
    .required()
    .pattern(regex('(0[1-9]|1[0-9]|2[0-9]|3[0-1])/(0[1-9]|1[0-2])/20[2-9][1-9]$'))
    .error(error('Fecha no valida, debe estar en el formato DD/MM/AAAA')),
  horaReserva: Joi.string()
    .pattern(regex('^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$'))
    .error(error('Fecha de la reserva no es valida')),
  puestos: Joi.number()
    .required()
    .min(1)
    .error(error('Se requiere tener al menos un asiento para reservar'))
})

/**
 * GET ALL method for Reservas
 */
ReservasDAO.get('/', (req, res) => {
  dbReservas.get().then(snapshot => {
    return res.status(200).send(snapshot.docs.map(doc => {
      return { ...doc.data(), id: doc.id }
    }))
  })
})

/**
 * GET BY ID method for Reservas
 */
ReservasDAO.get('/:idReserva', (req, res) => {
  const idReserva = req.params.idReserva
  dbReservas.doc(idReserva).get().then(
    doc => {
      if (doc.exists) {
        return res.status(200).send(doc.data())
      } else {
        return res.status(404).send('La reserva no existe')
      }
    }
  )
})

/**
 * POST method for Reservas
 */

const restauranteExiste = async (id:any) => {
  return (await dbRestaurantes.doc(id).get()).exists
}

const clienteExiste = async (id:any) => {
  return (await dbClientes.doc(id).get()).exists
}

const obtenerCliente = async (id:any) => {
  return (await dbClientes.doc(id).get()).data()
}

const obtenerRestaurante = async (id:any) => {
  return (await dbRestaurantes.doc(id).get()).data()
}

/** TODO: Arreglar Despues
const tiempoEntreReservas = async (horaReservada:Date, idCliente:any):Promise<boolean> => {
  return (await dbReservas.where('idClientes', '==', idCliente)
    .where('horaReserva', '>=', dateUtils.addHour(horaReservada, -1))
    .where('horaReserva', '<=', dateUtils.addHour(horaReservada, 1)).get()).empty
}
 */

const verificarCapacidad = async (puestosReserva:number, idRestaurante:any):Promise<boolean> => {
  const restaurante = (await dbRestaurantes.doc(idRestaurante).get()).data()
  return restaurante?.puestosReserva >= puestosReserva
}

const updateCapacidadRestaurante = async (puestos:number, idRestaurante:any) => {
  const restaurante = await obtenerRestaurante(idRestaurante)
  let disponibilidad = restaurante?.puestosReserva
  disponibilidad -= puestos
  return await dbRestaurantes.doc(idRestaurante).update(
    {
      puestosReserva: disponibilidad
    }
  )
}

const addReservasCliente = async (idCliente:any, idReserva: any) => {
  const cliente = await obtenerCliente(idCliente)
  const pedidos = cliente?.pedidos
  pedidos.push({ id: idReserva, tipo: 'Reserva' })
  return await dbClientes.doc(idCliente).update({ pedidos: pedidos })
}

const addReservasRestaurante = async (idRestaurante:any, idReserva: any) => {
  const restaurante = await obtenerRestaurante(idRestaurante)
  const pedidos = restaurante?.pedidos
  pedidos.push({ id: idReserva, tipo: 'Reserva' })
  return await dbRestaurantes.doc(idRestaurante).update({ pedidos: pedidos })
}

const removeReservasCliente = async (idCliente:any, idReserva: any) => {
  const cliente = await obtenerCliente(idCliente)
  const pedidos = cliente?.pedidos
  const index = pedidos.indexOf({ id: idReserva, tipo: 'Reserva' })
  pedidos.splice(index, -1)
  return await dbClientes.doc(idCliente).update({ pedidos: pedidos })
}

const removeReservasRestaurante = async (idRestaurante:any, idReserva: any) => {
  const restaurante = await obtenerRestaurante(idRestaurante)
  const pedidos = restaurante?.pedidos
  const index = pedidos.indexOf({ id: idReserva, tipo: 'Reserva' })
  pedidos.splice(index, -1)
  return await dbRestaurantes.doc(idRestaurante).update({ pedidos: pedidos })
}

ReservasDAO.post('/', async (req, res) => {
  const reserva = req.body
  const idRestaurante = reserva.restauranteId
  const idCliente = reserva.clienteId
  const { error } = reservaSchema.validate(reserva)
  if (!error) {
    switch (false) {
      case await restauranteExiste(idRestaurante): {
        return res.status(400).send('El restaurante no existe')
      }
      case await clienteExiste(idCliente): {
        return res.status(400).send('El cliente no existe')
      }
      case await verificarCapacidad(reserva.puestos, idRestaurante): {
        return res.status(400).send('El restaurante no tiene cupo disponible')
      }
      default: {
        const idReserva = (await dbReservas.add(reserva)).id
        addReservasCliente(idCliente, idReserva)
        addReservasRestaurante(idRestaurante, idReserva)
        updateCapacidadRestaurante(reserva.puestos, idRestaurante)
        return res.status(200).send({ message: 'La reserva se ha generado con exito', data: { ...reserva, id: idReserva } })
      }
    }
  } else {
    return res.status(400).send(error.message)
  }
})

/**
 * PUT method for Reservas
 */
ReservasDAO.put('/:idReserva', (req, res) => {
  const idReserva = req.params.idReserva
  const data = JSON.parse(req.body.data)
  dbReservas.doc(idReserva).get().then(
    async doc => {
      if (doc.exists) {
        req.body.horaReserva = dateUtils.createDateFromString(data.horaReserva)
        req.body.horaPedida = dateUtils.createDateFromString(data.horaPedida)
        const { error } = reservaSchema.validate(data)
        if (!error) {
          switch (false) {
            case await verificarCapacidad(-doc.data()?.puestos + data.puestos, data.restauranteId): {
              return res.status(400).send('El restaurante no tiene cupo disponible')
            }
          }
          updateCapacidadRestaurante(-doc.data()?.puestos, doc.data()?.restauranteId)
          updateCapacidadRestaurante(data.puestos, doc.data()?.restauranteId)
          dbReservas.doc(idReserva).update(data)
          return res.status(200).send('La reserva se ha actualizado con exito')
        } else {
          return res.status(400).send(error.message)
        }
      } else {
        return res.status(404).send('La reserva no existe')
      }
    }
  )
})

/**
 * DELETE
 */
ReservasDAO.delete('/:idReserva', (req, res) => {
  const idReserva = req.params.idReserva
  dbReservas.doc(idReserva).get().then(
    doc => {
      if (doc.exists) {
        removeReservasCliente(doc.data()?.clienteId, idReserva)
        removeReservasRestaurante(doc.data()?.restauranteId, idReserva)
        updateCapacidadRestaurante(-doc.data()?.puestos, doc.data()?.restauranteId)
        dbReservas.doc(idReserva).delete(req.body)
        return res.status(200).send('La reserva se ha eliminado')
      } else {
        return res.status(404).send('La reserva no existe')
      }
    }
  )
})
