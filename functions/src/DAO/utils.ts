/**
 * Class for managning DB connections
 */
import * as admin from 'firebase-admin'
import * as serviceAccount from './admin.json'

/**
 * Imports for distance calculation
 */
import 'reflect-metadata'
import { Distance, Geocoder, Location, GoogleMapsProvider, TravelModeEnum } from '@goparrot/geocoder'
import Axios, { AxiosInstance } from 'axios'
const geocodeAPIKEY = 'AIzaSyDboni-N2NAnMAQIlFbPBpAKdP-4Rxz4jE'
const axios: AxiosInstance = Axios.create()
const provider: GoogleMapsProvider = new GoogleMapsProvider(axios, geocodeAPIKEY)
const geocoder: Geocoder = new Geocoder(provider)

/**
 * Firebase credentials
 */
const credentials = {}
Object.assign(credentials, serviceAccount)

/**
  * Init app
  */
admin.initializeApp({
  credential: admin.credential.cert(credentials)
})

/**
  * Init DB
  */
export const databaseConnection = admin.firestore()

/**
 * Function that creates a RegExp
 * @param value Regex
 * @returns a regex object that follows the rule of the value
 */
export const regex = (value:string):RegExp => new RegExp(value)

/**
  * Function that creates an error
  * @param value error message
  * @returns an error containing the error message
  */
export const error = (value:string):Error => new Error(value)

/**
 * Function that searches by id in an array
 */
export const search = (id:any, arr:Array<any>) => arr.reduce((prev, curr) => {
  if (curr?.id === id) {
    prev = curr
    return prev
  } else if (prev && prev?.id === id) {
    return prev
  } else {
    return undefined
  }
})

/**
 * Function that searches de index of an object by id
 */
export const searchIndex = (id:any, arr:Array<any>):number => arr.map(x => x.id).indexOf(id)

export const evaualte = (arr:Array<boolean>) => arr.reduce((prev, curr) => prev && curr)

/**
 * Generate a random Id
 * @returns new Random Id
 */
export const generateRandomId = () => databaseConnection.collection('tmp').doc().id

/**
 * Functions with date utilities
 */
export const dateUtils = {

  /**
   * Create a date
   * @param dia
   * @param mes
   * @param anio
   * @param hora
   * @param minutos
   * @returns A date from an ISO format
   */
  createDateFromParams: (dia:string, mes:string, anio:string, hora:string, minutos:string) => {
    if (dia === '') {
      dia = '01'
    }
    if (hora === '') {
      hora = '01'
    }
    if (minutos === '') {
      minutos = '01'
    }
    return new Date(`${anio}-${mes}-${dia}T${hora}:${minutos}:00-5`)
  },

  /**
   * Create a date from a string
   * @param dia
   * @returns
   */
  createDateFromString: (dia:string) => {
    const [diaN, hora] = dia.split('T')
    return new Date(`${diaN}T${hora.split(':')[0]}:${hora.split(':')[1]}:00-5`)
  },
  createDateFromExpDate: (fechaExp:string) => {
    const [mes, anio] = fechaExp.split('/')
    return new Date(`20${anio}-${mes}-01T00:00:00-5`)
  },

  /**
   * Add or substract hours from a date
   * @param date
   * @param value
   * @returns
   */
  addHour: (date:Date, value:number) => {
    return new Date(date.getFullYear(), date.getMonth(), date.getDay(), date.getHours() + value, date.getMinutes(), 0)
  },

  /**
   * Reservation Hour to Hour String
   * @param date: Fecha
   * @returns
   */
  dateToHourString: (date:Date) => {
    return `${date.getHours()}:${date.getMinutes()}`
  },

  actualDate: () => {
    return new Date().toUTCString()
  }
}

export const encontrarElementoMasFrecuente = (arr:Array<any>) => {
  let compare = ''
  let mostFreq = ''

  arr.reduce((acc:any, val:any) => {
    if (val in acc) { // if key already exists
      acc[val]++ // then increment it by 1
    } else {
      acc[val] = 1 // or else create a key with value 1
    }
    if (acc[val] > compare) { // if value of that key is greater
      // than the compare value.
      compare = acc[val] // than make it a new compare value.
      mostFreq = val // also make that key most frequent.
    }
    return acc
  }, {})
  return mostFreq
}

export const gpsUtils = {

  /**
   * Get coordinates of an addres
   * @param address
   * @returns String as: latitude;longitude
   */
  getCoordinates: async (address:string) => {
    const locations: Location[] = await geocoder.geocode({
      address: address
    })
    console.log('----------------------------------')
    return `${locations[0].latitude};${locations[0].longitude}`
  },

  /**
   * Calculate distance from coordOne to coordTwo by car
   * @param coordinateOne
   * @param coordinateTwo
   * @returns Distance between two location in Km
   */
  getDistance: async (coordinateOne:String, coordinateTwo:String) => {
    const origin = coordinateOne.split(';').map(x => Number.parseFloat(x))
    const destination = coordinateTwo.split(';').map(x => Number.parseFloat(x))
    const distance: Distance = await geocoder.distance({
      from: {
        lat: origin[0],
        lon: origin[1]
      },
      to: {
        lat: destination[0],
        lon: destination[1]
      },
      mode: TravelModeEnum.DRIVING
    })
    return await (distance.distance / 1000).toFixed(2)
  }
}
