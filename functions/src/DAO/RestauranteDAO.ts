/* eslint-disable prefer-regex-literals */
/**
 * DAO class for Restaurante
 */
import {
  databaseConnection,
  regex,
  error,
  search,
  generateRandomId,
  searchIndex,
  gpsUtils
} from './utils'
import * as Joi from 'joi'
import * as express from 'express'

const db = databaseConnection.collection('restaurantes')

/**
  * Express App for handeling request
  */
export const RestauranteDAO = express()

/**
  * Joi restaurantSchema
  */
const restaurantSchema = Joi.object({
  nombre: Joi.string()
    .required()
    .max(100)
    .min(5)
    .pattern(regex('^[A-Z|a-z|0-9| ]*$'))
    .error(error('Nombre no valido')),
  correo: Joi.string()
    .required()
    .email()
    .error(error('Correo no valido')),
  telefono: Joi.string()
    .required()
    .length(10)
    .pattern(regex('^[0-9]*$')),
  nit: Joi.string()
    .required()
    .min(7)
    .pattern(regex('^[0-9]*$')),
  direccion: Joi.string()
    .required()
    .min(7)
    .error(error('Direcccion no valida')),
  pedidos: Joi.array()
    .required()
    .min(0)
    .error(error('No hay propiedad para los pedidos')),
  platos: Joi.array()
    .required()
    .min(0)
    .error(error('No hay propiedad para los pedidos')),
  horariosReserva: Joi.array()
    .required()
    .items(Joi.string().pattern(regex('^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$')))
    .min(0)
    .error(error('No se especificaron horarios para la reserva del restaurante')),
  puestosReserva: Joi.number()
    .required()
    .min(0)
    .error(error('Se tiene que especificar el numero de puestos de reserva')),
  disponibleTTG: Joi.string()
    .required()
    .valid(...['true', 'false'])
    .error(error('No se especifico correctamente si el restaurante toma ordenes para llevar')),
  rating: Joi.array()
    .items(Joi.number().min(0).max(5))
    .required()
    .min(0)
    .error(error('El rating no se encuentra entre el limite especificado (0-5)')),
  ciudad: Joi.string()
    .required()
    .pattern(regex('^[A-Z|a-z|0-9| ]*$'))
    .error(error('Ciudad')),
  foto: Joi.string()
    .required()
    .uri()
    .error(error('Direccion a la foto no es valdia'))
})

/**
 * JOI Schema for Dishes
 */
const dishSchema = Joi.object({
  nombre: Joi.string()
    .required()
    .min(5)
    .pattern(regex('^[A-Z|a-z| ]*$'))
    .error(error('Nombre del plato no valido')),
  precio: Joi.number()
    .required()
    .min(1000)
    .error(error('Precio no valido'))
})

/**
  * GET ALL Method for Restaurants
  * ObtenerDistanciasAUnPunto: https://{url}/restaurante?distancia=[coord]
  * ObtenerPorEmail: https://{url}/restaurante?email=email
  * OrdenarRaiting:  https://{url}/restaurante?order=rating
  * OrdenarPosicion: https://{url}/restaurante?order=posicion&pos=4.12321;-74.0344  Separador Decimal es .
  */
RestauranteDAO.get('/', async (req, res) => {
  const docEmail = req.query.email
  const order = req.query.order
  const distancia = req.query.distacia
  let restaurantes = (await db.get()).docs.map(snapshot => { return { ...snapshot.data(), id: snapshot.id } })
  switch (order) {
    case 'rating': {
      restaurantes = ordenarRestaurantesPorRaiting(restaurantes)
      break
    }
    case 'posicion': {
      restaurantes = await ordenarRestaurantesPorPosicion(restaurantes, req.query.pos)
      break
    }
    default: {
      if (docEmail) {
        return getByEmail(docEmail, res)
      }
      if (distancia) {
        return getDistanciaRestaurante(restaurantes, req.query.distacia, res)
      }
      break
    }
  }
  return res.status(200).send(restaurantes)
})

const getDistanciaRestaurante = (restaurantes:Array<any>, pos:any, res:any) => {
  const restaurantesDistancia = restaurantes.map(restaurante => { return { distancia: gpsUtils.getDistance(pos, restaurante.coord), restaurante: restaurante } })
  return res.status(200).send(restaurantesDistancia)
}

/**
 * Ordena la lista de restaurantes por raiting
 * @param restaurantes
 * @returns
*/
const ordenarRestaurantesPorRaiting = (restaurantes:Array<any>):Array<any> => {
  const restaurantesKey = restaurantes.map(restaurante => { return { key: calcularRatingRestaurante(restaurante), restaurante: restaurante } })
  restaurantesKey.sort((n1, n2) => {
    if (n1.key < n2.key) {
      return 1
    } if (n1.key > n2.key) {
      return -1
    }
    return 0
  })
  return restaurantesKey.map(object => object.restaurante)
}

/**
 * Ordena la lista de restaurantes por posicion
 * Reverse Geocoding for Android https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,
+Mountain+View,+CA&key=AIzaSyAHFHleCtbN81we9LUDMQ2eGRarf2ncpFs
 * @param restaurantes
 * @param coordenadas
 * @returns
 */
const ordenarRestaurantesPorPosicion = async (restaurantes:Array<any>, coordenadas:any) => {
  const restaurateOrden = restaurantes.map(restaurante => { return { key: gpsUtils.getDistance(coordenadas, restaurante.coord), data: restaurante } })
  restaurateOrden.sort((n1, n2) => {
    if (n1.key < n2.key) {
      return 1
    } if (n1.key > n2.key) {
      return -1
    }
    return 0
  }
  )
  return restaurateOrden.map(object => object.data)
}

const calcularRatingRestaurante = (restaurante:any): number => {
  return restaurante.rating.reduce((acc: any, curr: any) => acc + curr) / restaurante.rating.length
}

/**
  * GET BY ID Method for Restaurants
  */
RestauranteDAO.get('/:id', (req, res) => {
  const docId = req.params.id
  db.doc(docId).get().then((doc) => {
    if (doc.exists) {
      return res.status(200).send({ ...doc.data(), id: doc.id })
    } else {
      return res.status(404).send('Restaurante no encontrado')
    }
  }
  )
})

/**
 * GET BY EMAIL Method for Restaurant
 */
const getByEmail = async (docEmail:any, res:any) => {
  db.where('correo', '==', docEmail)
    .get()
    .then((snapshot) => {
      if (!snapshot.empty) {
        const doc = snapshot.docs[0]
        return res.status(200).send({ ...doc.data(), id: doc.id })
      } else {
        return res.status(400).send('No se encontro un restaurante con ese correo')
      }
    })
}

/**
  * POST Methof for Restaurants
  */
const validarUnicoTelefono = async (telefono:string) => {
  return (await db.where('telefono', '==', telefono).get()).empty
}

const validarUnicoCorreo = async (correo:string) => {
  return (await db.where('correo', '==', correo).get()).empty
}

const validarUnicoDocumento = async (documento:string) => {
  return (await db.where('nit', '==', documento).get()).empty
}

RestauranteDAO.post('/', async (req, res) => {
  const data = req.body
  const RestauranteDoc = data.nit.trim()
  const telefonoRestaurante = data.telefono.trim()
  const correoRestaurante = data.correo.trim()
  const { error } = restaurantSchema.validate(data)
  if (!error) {
    switch (false) {
      case await validarUnicoDocumento(RestauranteDoc): {
        return res.status(400).send('Existe un restaurante con el NIT dado')
      }
      case await validarUnicoTelefono(telefonoRestaurante): {
        return res.status(400).send('Existe un restaurante con el telefono dado')
      }
      case await validarUnicoCorreo(correoRestaurante): {
        return res.status(400).send('Existe un restaurante con el correo dado')
      }
      default: {
        const id = await (await (await db.add(data)).get()).id
        const coord = await gpsUtils.getCoordinates(`${data.direccion},${data.ciudad},Colombia`)
        return res.status(200).send({ ...data, id: id, coord: coord })
      }
    }
  } else {
    console.log(error.message)
    return res.status(400).send(error.message)
  }
})

/**
  * PUT method for Restaurant
  */
RestauranteDAO.put('/:id', (req, res) => {
  const docId = req.params.id
  const data = JSON.parse(req.body.data)
  db.doc(docId)
    .get()
    .then((doc) => {
      if (doc.exists) {
        const { error } = restaurantSchema.validate(data)
        if (!error) {
          db.doc(docId).set(data)
          return res.status(200).send({ message: 'Restaurante actualizado exitosamente', data: { ...data, id: docId } })
        } else {
          return res.status(400).send(error.message)
        }
      } else {
        return res.status(404).send('El restaurante no fue encontrado')
      }
    }
    )
})

/**
  * DELETE method for Restaurant
  */
RestauranteDAO.delete('/:id', (req, res) => {
  const docId = req.params.id
  db.doc(docId).get().then((doc) => {
    if (doc.exists) {
      db.doc(docId).delete()
      return res.status(200).send('Restaurant Deleted')
    } else {
      return res.status(404).send('Restaurant not found')
    }
  }
  )
})

/**
 * GET ALL Method for a restaurant dishes
 */
RestauranteDAO.get('/:id/plato/', (req, res) => {
  const docId = req.params.id
  db.doc(docId).get().then((doc) => {
    if (doc.exists) {
      return res.status(200).send(doc.data()?.platos.map((plato:any) => { return { ...plato, id: plato.id } }))
    } else {
      return res.status(404).send('Restaurante no encontrado')
    }
  }
  )
})

/**
 * GET BY ID Method for a restaurant dish
 */

const verficarRestaurante = async (id:any) => {
  return (await db.doc(id).get()).exists
}

const obtenerRestaurante = async (id:any) => {
  return (await db.doc(id).get()).data()
}

RestauranteDAO.get('/:id/plato/:idPlato', async (req, res) => {
  const docId = req.params.id
  const idPlato = req.params.idPlato
  const restaurante = await obtenerRestaurante(docId)
  const platos = restaurante?.platos
  const plato = search(idPlato, platos)
  if (await verficarRestaurante(docId)) {
    if (!plato) {
      return res.status(400).send('El plato requerido no existe')
    } else {
      return res.status(200).send({ ...plato, id: plato.id })
    }
  } else {
    return res.status(400).send('El restaurante no existe')
  }
})

/**
 * POST Method for a restaurant dish
 */
RestauranteDAO.post('/:id/plato/', async (req, res) => {
  const data = req.body
  const docId = req.params.id
  const restaurant = await obtenerRestaurante(docId)
  const platos = restaurant?.platos
  const { error } = dishSchema.validate(data)
  if (await verficarRestaurante(docId)) {
    if (!error) {
      const id = generateRandomId()
      platos.push({ ...data, id: id })
      db.doc(docId).update({ platos: platos })
      return res.status(200).send({ message: 'Plato creado exitosamente', data: { ...data, id: id } })
    } else {
      return res.status(400).send(error.message)
    }
  } else {
    return res.status(400).send('Restaurante no encontrado')
  }
})

/**
 * PUT Method for a restaurant dish
 */
RestauranteDAO.put('/:id/plato/:idPlato', async (req, res) => {
  const data = JSON.parse(req.body.data)
  const docId = req.params.id
  const idPlato = req.params.idPlato
  const restaurant = await obtenerRestaurante(docId)
  const platos = restaurant?.platos
  const plato = search(idPlato, platos)
  const { error } = dishSchema.validate(data)
  if (await verficarRestaurante(docId)) {
    if (!error) {
      if (!plato) {
        return res.status(400).send('Plato no encontrado')
      } else {
        const index = searchIndex(idPlato, platos)
        platos[index] = { ...data, id: idPlato }
        db.doc(docId).update({ platos: platos })
        return res.status(200).send('Plato actualizado')
      }
    } else {
      return res.status(400).send(error.message)
    }
  } else {
    return res.status(400).send('Cliente no encontrado')
  }
})

/**
 * DELETE method for a restaurant dish
 */
RestauranteDAO.delete('/:id/plato/:idPlato', async (req, res) => {
  const docId = req.params.id
  const idPlato = req.params.idPlato
  const restaurant = await obtenerRestaurante(docId)
  const platos = restaurant?.platos
  const plato = search(idPlato, platos)
  if (await verficarRestaurante(docId)) {
    if (!plato) {
      return res.status(400).send('Plato no encontrado')
    } else {
      db.doc(docId).update({
        platos: platos.splice(searchIndex(idPlato, platos), 1)
      })
      return res.status(200).send('El plato ha sido eliminado')
    }
  } else {
    return res.status(400).send('Restaurante no encontrado')
  }
})
