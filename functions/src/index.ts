import * as functions from 'firebase-functions'
import { ClienteDAO } from './DAO/ClienteDAO'
import { RestauranteDAO } from './DAO/RestauranteDAO'
import { ReservasDAO } from './DAO/ReservaDAO'
import { recomendacionesManager } from './DAO/recomendaciones'

export const cliente = functions.https.onRequest(ClienteDAO)
export const restaurante = functions.https.onRequest(RestauranteDAO)
export const reserva = functions.https.onRequest(ReservasDAO)
export const recomendaciones = functions.https.onRequest(recomendacionesManager)
