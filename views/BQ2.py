import pandas as pd;
import matplotlib.pyplot as plt;
import requests;
import numpy as np;

# URL de donde se obtinen los datos del backend (por ahora localmente)
URL = 'https://us-central1-ordersapp202120.cloudfunctions.net/restaurante'
# Realizando peticion GET
r = requests.get(URL)
# Convirtiendo datos obtenidos a formato JSON
data = r.json()

print(data)
# Se considera (bajo el modelo actual) que un restaurante está en un área si su dirección inicia por el area dada
dic = []
names = []
values = []
for restaurant in data:
    ratings = restaurant['rating']
    rat = 0
    for r in ratings:
        print(r)
        rat += r
    rating = rat/len(ratings)
    names.append(restaurant['nombre'])
    values.append(rating)

fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
print(values)

ax.bar(names,values)
plt.show()

