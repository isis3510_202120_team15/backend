import requests;

# URL de donde se obtinen los datos del backend (por ahora localmente)
URL = 'http://localhost:5001/ordersapp202120/us-central1/cliente'

# Realizando peticion GET
r = requests.get(URL)
# Convirtiendo datos obtenidos a formato JSON
data = r.json()

users_count = len(data)


print("Total of users using the app:", users_count)