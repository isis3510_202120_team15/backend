from matplotlib import rc_file_defaults
import pandas as pd;
import matplotlib.pyplot as plt;
import requests;
import matplotlib.dates as mdates;
import time;
import numpy as np;

# URL de donde se obtinen los datos del backend (por ahora localmente)
URL = 'https://us-central1-ordersapp202120.cloudfunctions.net/reserva'
URL_rest = 'https://us-central1-ordersapp202120.cloudfunctions.net/restaurante'
# Realizando peticion GET
r = requests.get(URL)
rest= requests.get(URL_rest)
# Convirtiendo datos obtenidos a formato JSON
data = r.json()
data_rest = rest.json()
restaurantes = {}



#restaurantes
for i in data_rest:
   if(i['nombre'] not in restaurantes):
       restaurantes[i['nombre']] = i['id']
      #restaurantes.append(i['id'])


#horas
horas = []
for i in data:
    if(i['horaReserva']not in horas):
        horas.append(i['horaReserva'])
horas.sort()

#reservas por hora
reservasPorHora = np.zeros((len(restaurantes),len(horas)))
cont =0 
for i in restaurantes.values():
    for  j in range(len(horas)):
        for k in data:
            if (k['restauranteId']==i and k['horaReserva']==horas[j] ):
                reservasPorHora[cont,j] += 1
    cont+=1

# maximos por fila
maximos=[]
for i in reservasPorHora:
    max_res = np.amax(i)
    maximos.append(max_res)

# mostrar
fig, ax = plt.subplots() 
ax.set_axis_off() 
table = ax.table( 
    cellText = reservasPorHora,  
    rowLabels = list(restaurantes.keys()),  
    colLabels = horas, 
    rowColours =["palegreen"] * 10,  
    colColours =["palegreen"] * 10, 
    cellLoc ='center',  
    loc ='upper left')         
   
ax.set_title('Hours people book up more frequently a table by restaurant', 
             fontweight ="bold") 
   
for i in range(len(reservasPorHora)):
    for j in range(len(reservasPorHora[i])):
        if (reservasPorHora[i][j]==maximos[i]):
            table[(i+1, j)].set_facecolor("#ff0000")
plt.show() 


