import pandas as pd;
import matplotlib.pyplot as plt;
import requests;
import matplotlib.dates as mdates;
import time;
import numpy as np;

# URL de donde se obtinen los datos del backend (por ahora localmente)
URL = 'https://us-central1-ordersapp202120.cloudfunctions.net/reserva'
# Realizando peticion GET
r = requests.get(URL)
# Convirtiendo datos obtenidos a formato JSON
data = r.json()

dic = {'Mon':0,'Tue':0,'Wed':0,'Thu':0,'Fri':0,'Sat':0,'Sun':0}    

for i in data:
    fecha = i.get('diaReserva')
    if fecha:
        tiempo = time.asctime((time.strptime(fecha,"%d/%m/%Y")))
        day = tiempo[0:3]
        dic[day] += 1
    
fig, ax = plt.subplots(1,1)

ax.bar(dic.keys(),dic.values())
ax.set_title('Day of reservetion')

plt.show()